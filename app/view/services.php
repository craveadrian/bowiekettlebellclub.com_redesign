<div id="content">
	<div class="row services">
		<h1>SERVICES</h1>
    	
    	<dl id="kettlebell">
    		<dt>Unique Kettlebell & Strength Training</dt>
    		<dd>
    			<p>At the Bowie <a href="public/pdf/kettlebell1.pdf" target="_blank">Kettlebell</a> Club in Bowie, Maryland, you’ll find <a href="public/pdf/kettlebell2.pdf" target="_blank">authentic kettlebell and strength training</a> that will improve your joint mobility, functional movement, and help you build strength. Primarily geared at those ages 35 to 60, our classes range from 40 minutes to one hour, and they’re assigned on an individual basis. If you’re interested in kettlebell training, you must sign a waiver and provide a doctor’s note if you have special health needs. Our strength training courses are either five or <a href="public/pdf/kettlebell3.pdf" target="_blank">10 one-hour sessions</a> and is conducted in studio. We also have a <a href="public/pdf/kettlebell4.pdf" target="_blank">fat burning workout</a> and <a href="public/pdf/kettlebell5.pdf" target="_blank">Kettlebell Blast</a> you can try.</p>

    			<b>Contact us to learn more about kettlebell and strength training, or to inquire about rates.</b>
    		</dd>

    		<dl class="links">
    			<dt>Links</dt>
    			<dd>
    				<ol>
    					<li><a href="http://vimeo.com/marvinkingrkc/videos" target="_blank">Video</a></li>
    					<li><a href="public/pdf/waiver.pdf" target="_blank">Consent Form</a></li>
    					<li><a href="https://www.dragondoor.com/store/" target="_blank">Kettlebell Store</a></li>
    					<li><a href="https://www.dragondoor.com/" target="_blank">Dragon Door and the RKC</a>
    					<p>Everything you need on Kettlebells, Strenght Training, and the RKC</p>
    					</li>
    					<li><a href="https://www.dragondoor.com/marvin_king_rkc_ncsf_cpt/" target="_blank">Refferals and Testimonials</a>
    						<p>Click to see what others have to say about Marvin’s unique approach to teaching Kettlebells.</p>
    					</li>
    				</ol>
    			</dd>
    		</dl>
    	</dl>

    	<dl id="self_training">
    		<dt>Effective Self-Defense Training</dt>
    		<dd>
    			<p>t the Bowie Kettlebell Club in Bowie, Maryland, you’ll find effective self-defense training based on reality situations, not a staged or practices event. Students must be over 18 years-of-age for the training, and each class is one to two hours. Each class has a four-student minimum. Our self-defense training classes are assigned on an individual basis and student will sign a waiver. Students with special health needs must have a doctor’s approval. All training is conducted in our studio using real-life situations.</p>

    			<b>Contact us in Bowie, Maryland, to learn more about our unique self-defense training.</b>
    			<br><br>
    			<p>Here is a list of our favorite links about Target Focus Training. We hope these links help you understand the reason when to apply TFT to protect yourself. If you have suggestions about other sites to include, send email to <?php $this->info(["email", "mailto"]) ?></p>

    			<ol>
    				<li><a href="public/pdf/waiver.pdf" target="_blank">Consent Form</a></li>
    				<li><a href="http://www.cbsnews.com/news/when-is-violence-necessary-to-protect-yourself/" target="_blank">Tim Larkin’s CBS Interview</a>
    					<p>Tim Larkin explains when to use TFT.</p>
    				</li>
    				<li><a href="http://goo.gl/7n2a9u" target="_blank">Tim Larkin’s Katie Couric Interview</a>
    					<p>Tim Larkin (Creator of Target Focus Training) gives explanation on what TFT is about.</p>
    				</li>
    				<li><a href="http://www.targetfocustraining.com/" target="_blank">Target Focus Training (TFT)</a> <p>www.targetfocustraining.com</p></li>
    				<li><a href="http://timlarkin.com/bookspecial/video.php" target="_blank">Free Self Protection Video</a><p>Womens self protection senario training.</p></li>
    			</ol>
    		</dd>

    	</dl>



	</div>
</div>
