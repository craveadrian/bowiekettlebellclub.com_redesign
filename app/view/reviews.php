<div id="content">
	<div class="row">
		<h1>REVIEWS</h1>
    	
    	<div class="reviews">
    		<span class="stars">&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</span>
    		<h3>A fantastic full body workout</h3>
    		<p>I began taking Marvin's kettle bell class after a surgery. Within a few months he had me back to my previous physical condition and working on improving my run time. In addition he took time to help me achieve personal goals and work on improving my Turkish Getup.</p>

    		<p><b>- Margy Holston</b> / Silver Spring, USA</p>
    	</div>

    	<div class="reviews">
    		<span class="stars">&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</span>
    		<h3>Fantastict Workout to Achieve Fitness Goals</h3>
    		<p>I started taking a kettlebells class with Marvin earlier this year. I have found it to be a fantastic workout and an excellent compliment to my current workout routine. Marvin takes the time to break down each of the moves and provides great support and encouragement for helping everyone reach or maintaining their fitness goals. I can wholeheartedly recommend this workout, no matter what your fitness level.</p>

    		<p><b>- Susan W</b> / Silver Spring, USA</p>
    	</div>

    	<div class="reviews">
    		<span class="stars">&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</span>
    		<h3>Great instructor</h3>
    		<p>Marvin is a world class RKC instructor who was taught by Pavel himself. He is a great guy, patient and good hearted soul who embodies what strength is all about. With his background in martial arts, running and his reputable RKC training, I will not hesitate in referring any of my patients to him or anyone looking for quality kettlebell training. Dat Quach, Doctor of Physical Therapy, OCS, HKC</p>

    		<p><b>- Dat Quach</b> / Bowie, United States</p>
    	</div>

    	<div class="reviews">
    		<span class="stars">&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</span>
    		<h3>Gifted teacher</h3>
    		<p>Marvin's approach to Kettlebells is a blend of East and West and has increased my functional strength right from the start. At age 59, I didn't think I'd ever last in a KB class and now, over 3 years later, I still learn new things and enjoy the class. Needless to say I'm stronger, more flexible and have better neuromuscular integration than many individuals much younger than I am - which feeds my vanity and keeps my medical costs low. I can't say enough positive things about his classes. Try a couple of classes and see for yourself!</p>

    		<p><b>- Mary K</b> / Silver Spring, USA</p>
    	</div>

    	<div class="reviews">
    		<span class="stars">&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</span>
    		<h3>Patience + form = results without injury</h3>
    		<p>I enjoy strength training but don't like emphasis on reps or weight over doing the exercise correctly. Marvin is all about establishing proper form first and then going up in weight and reps. He is constantly thinking of new ways to teach the body the subtle cues I need to do the exercise correctly, and he's willing to try anything. Under his tutelage, I have steadily increased the weight I'm handling without ever having suffered an injury related to his class. The strength isn't just for kettlebells, either - I'm a stronger runner, too, and in everyday tasks I can feel the difference as well. I've also lost a little weight. I feel quite good about having taken Marvin's classes and plan to continue doing so.</p>

    		<p><b>- Andrew Malone</b> / Silver Spring, USA</p>
    	</div>

    	<div class="reviews">
    		<span class="stars">&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</span>
    		<h3>I've never felt this strong in my life!</h3>
    		<p>I have been taking kettlebell classes with Marvin king since Jan 2013 and I can see amazing changes in my body and I am loving the way that I feel! I am learning so much about Kettlebells and hope to continue successfully on this journey! <br> Thanks Marvin!</p>

    		<p><b>- Judy Hurbon	</b> / Silver Spring, USA</p>
    	</div>

    	<div class="reviews">
    		<span class="stars">&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</span>
    		<h3>Highly Recommend!</h3>
    		<p>My husband & I both had the pleasure of training with Marvin King at Blue Heron. We had never worked out together before usually because we had different routines, goals, etc. But all that changed with kettlebells and with Marvin. Both my husband & I enjoyed the workout and all of the stretches we did in the beginning of class. Marvin was knowledgeable, helpful, and an all around nice guy. He explained everything including proper technique and things to avoid. If we had any questions or concerns, Marvin addressed them. We recently moved to Arizona and we really miss training with him.</p>

    		<p><b>- Jodi Wick</b> / Phoenix, United States</p>
    	</div>

    	<div class="reviews">
    		<span class="stars">&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</span>
    		<h3>Technical expertise and great enthusiasm</h3>
    		<p>I've been taking Marvin's weekly kettlebells class for a couple of years now. He is a patient instructor who is concerned about our ability to maintain proper form to avoid injury. He also explains how various kettlebell moves are applicable to how we move in everyday life and will help us maintain strength and agility as we age. I follow Marvin's various workouts when I practice kettlebells at home. Kettlebells have taken my fitness to a new level, thanks to Marvin's technical expertise and great enthusiasm.</p>

    		<p><b>- Natalie D.</b> / Silver Spring, MD, U.S.</p>
    	</div>

    	<div class="reviews">
    		<span class="stars">&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</span>
    		<h3>Health as well as strength.</h3>
    		<p>Kettlebells are medicine balls in a different form. After some health set-backs, Marvin introduced me to kettle bells. With their use and proper diet, I am regaining my strength and health. The many benefits are great to read about, but nothing compares with the actual "doing" kettlebells. Not a medical endorsement, but kettlebells have something for everyone. Give them a try.</p>

    		<p><b>- John Harris</b> / Takoma Park, MD USA</p>
    	</div>

    	<div class="reviews">
    		<span class="stars">&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</span>
    		<h3>Enthusiastic, Knowledgeable Teacher</h3>
    		<p>I knew exactly nothing about KB;books and DVDs are not enough to learn the subtleties of the training, so I was fortunate to hear about Mr. King from a recommendation. He is a highly knowledgeable and enthusiastic teacher who was able to fit the instruction to my particular abilities. I appreciate the emphasis he places on good form, technique and safety. He also shows the applicability of KB training to my activities. I highly recommend him to those interested in an introduction to KB training.</p>

    		<p><b>- Tom Adams</b> / Crofton, MD</p>
    	</div>

    	<div class="reviews">
    		<span class="stars">&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</span>
    		<h3>Marvin's Instruction was Great!</h3>
    		<p>Marvin, <br> Thanks for teaching the intro and intermediate Kettle Bell workshops at KMDC last Saturday at Gallery Place. The kettle bell exercises were a heck of a workout and the instruction was great. My nagging lower back injury is already starting to feel better just by learning how to properly move and strenghten my hips and abs. I am definitely going to be incorporating kettle bells into my workout routine. Hopefully KMDC will get a kettle bell class or two added to their schedule so that everybody can see the benefits of kettle bells. <br>Thanks again</p>

    		<p><b>- Doug Bachler </b> / USA</p>
    	</div>

    	<div class="reviews">
    		<span class="stars">&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</span>
    		<h3>Develops Individual Workout Programs</h3>
    		<p>Marvin designs great individual workout programs. The workout programs are structured to include using proper techniques, developing and strenthening muscles, and proper breathing techniques. I suffer from chronic lower back pain. Marvin has designed a plan for stretching and strenthening my lower back which has helped to significantly limit the pain. I highly recommend Marvin as a Personal Trainer and promise you will not be disappointed (perhaps a bit sore but its worth it).</p>

    		<p><b>- Vanessa</b> / Maryland</p>
    	</div>

    	<div class="reviews">
    		<span class="stars">&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</span>
    		<h3>I have known Marvin for years... He is great!!!</h3>
    		<p>Marvin and I would stretch out together 35 years ago. He was always patient and helpful. He maintained his passion of what he loved and never looked back. Marvin is absolutely passionate about helping others to become fit, and understand their bodies. Thank you Marvin for being such a role model to me. May you and your wonderful family be continuously blessed. <br>Love your little sister <br>Caroline (Carrie) King Brooks RN, BSN <br>"God Bless our Veterans..." <br>(That includes you ...) <br>11/13/08</p>

    		<p><b>- Caroline King Brooks RN, BSN</b> / Cleveland, Ohio</p>
    	</div>

    	<div class="reviews">
    		<span class="stars">&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</span>
    		<h3>Stresses Proper Technique!</h3>
    		<p>Marvin understands the need to stress good posture and proper technique, but was hoping for more demos and less of a lecture.</p>

    		<p><b>- Luke</b> / USA</p>
    	</div>

    	<div class="reviews">
    		<span class="stars">&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</span>
    		<h3>A Great Workout!</h3>
    		<p>I've used Kettlebells in cross fit workouts, but this Kettlebell Training is a great workout all it's own!</p>

    		<p><b>- Brandy</b> / USA</p>
    	</div>

    	<div class="reviews">
    		<span class="stars">&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</span>
    		<h3>Excellent Instruction!</h3>
    		<p>Marvin is an excellent instructor. Very approchable and patient. Thanks for a great Kettlebell class!</p>

    		<p><b>- Jessica</b> / USA</p>
    	</div>

	</div>
</div>
