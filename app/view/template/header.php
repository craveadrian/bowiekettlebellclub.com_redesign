<?php $this->suspensionRedirect($view); ?>
<!DOCTYPE html>
<html lang="en" <?php $this->helpers->htmlClasses(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<?php $this->helpers->seo($view); ?>
	<link rel="icon" href="public/images/favicon.png" type="image/x-icon">
	<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<link href="<?php echo URL; ?>public/styles/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<link rel="stylesheet" href="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.css" media="screen" />
	<?php $this->helpers->analytics(); ?>
</head>

<body <?php $this->helpers->bodyClasses($view); ?>>
<?php $this->checkSuspensionHeader(); ?>
	<header>
		<div id="header">
			<div class="row">
				<div class="hdLeft inb">
					<a href="<?php echo URL ?>"> <img src="public/images/common/mainLogo.png" alt="<?php $this->info("company_name");?> LOGO"> </a>
				</div>
				<div class="hdRight inb">
					<div class="hdRightTop">
						<div class="info">
							<p>
								<a target="_blank" href="<?php $this->info("fb_link") ?>" class="socialico">F</a>
								<a target="_blank" href="<?php $this->info("tt_link") ?>" class="socialico">L</a>
								<a target="_blank" href="<?php $this->info("yt_link") ?>" class="socialico">X</a>
								<a target="_blank" href="<?php $this->info("rss_link") ?>" class="socialico">R</a>
							</p>	
						</div>
						<div class="info">
							<img src="public/images/common/mail.png" alt="Email Icon" class="inb">
							<p class="inb"><?php $this->info(["email","mailto"]); ?></p>
						</div>
						<div class="info">
							<img src="public/images/common/phone.png" alt="Phone Icon" class="inb">
							<p class="phone inb"><?php $this->info(["phone","tel"]); ?></p>
						</div>
					</div>
					<div class="hdRightBot">
						<nav>
							<a href="#" id="pull">
								<strong>MENU</strong>
								<img src="public/images/nav-icon.png" alt="nav icon">
							</a>
							<ul>
								<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">HOME</a></li>
								<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services#content">SERVICES</a></li>
								<li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery#content">GALLERY</a></li>
								<li <?php $this->helpers->isActiveMenu("reviews"); ?>><a href="<?php echo URL ?>reviews#content">REVIEWS</a></li>
								<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact#content">CONTACT US</a></li>
								<li <?php $this->helpers->isActiveMenu("privacy-policy"); ?>><a href="<?php echo URL ?>privacy-policy#content">PRIVACY POLICY</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</header>

	<?php //if($view == "home"):?>
		<div id="banner">
			<div class="row">
				<div class="container">
					<div class="left inb">
						<a href="<?php echo URL ?>"> <img src="public/images/common/bannerLogo.png" alt="<?php $this->info("company_addess"); ?> LOGO"> </a>
					</div>
					<div class="right inb">
						<div class="box">
							<p>FREE CLASS</p>
							<p>Offer Valid On Your First 40-Minute Introductory Class New Sessions Daily!</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php //endif; ?>
