<footer>
	<div id="footer">
		<div class="ftContact">
			<div class="row">
				<div class="map"><iframe width="100%" height="417px" src="https://maps.google.com/maps?width=720&amp;height=600&amp;hl=en&amp;q=Bowie%2C%20MD%2020715+(BOWIE%20KETTLEBELL%20CLUB)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/create-google-map/">Embed Google Map</a></iframe></div><br />
				<h1>CONTACT US</h1>
				<form action="sendContactForm" method="post"  class="sends-email ctc-form home-form" >
					<div class="ctcLeft inb">
						<div class="row-1">
							<div class="col-1 inb">
								<input type="text" name="name" placeholder="Name:">
								<input type="text" name="email" placeholder="Email:">
							</div>
							<div class="col-2 inb">
								<input type="text" name="phone" placeholder="Phone:">
								<input type="text" name="subject" placeholder="Subject:">
							</div>
						</div>
						<div class="row-2">
							<textarea name="message" cols="30" rows="10" placeholder="Message:"></textarea>
							<button type="submit" class="ctcBtn btn ctcbot" disabled>SEND</button>
							<div class="g-recaptcha ctcbot"></div>
						</div>
					</div>
					<div class="ctcRight inb">
						<div class="col-1 inb">
							<input type="checkbox" name="consent" class="consentBox ckcinb"><p class="ckText ckcinb">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.</p><br>
							<?php if( $this->siteInfo['policy_link'] ): ?>
							<input type="checkbox" name="termsConditions" class="termsBox ckcinb"/><p class="ckText ckcinb"> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a></p>
							<?php endif ?>
						</div>
						<div class="col-2 inb">
							<p>Address: <span><a href="https://www.google.com.ph/maps?q=Bowie,+MD+20715&um=1&ie=UTF-8&sa=X&ved=0ahUKEwiw69Sv_bTcAhWGZlAKHYylCWkQ_AUICigB" target="_blank"><?php $this->info("address"); ?></a></span> </p>
							<p>Phone: <span><?php $this->info(["phone", "tel"]); ?></span> </p>
							<p>Email: <span><?php $this->info(["email", "mailto"]); ?></span> </p>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="footBot">
			<div class="row">
				<p class="copy">
					© <?php echo date("Y"); ?>. <?php $this->info("company_name"); ?>. All Rights Reserved.
					<?php if( $this->siteInfo['policy_link'] ): ?>
						<a href="<?php $this->info("policy_link"); ?>">Privacy Policy</a>.
					<?php endif ?>
				</p>
				<p>
					<a target="_blank" href="<?php $this->info("fb_link") ?>" class="socialico">F</a>
					<a target="_blank" href="<?php $this->info("tt_link") ?>" class="socialico">L</a>
					<a target="_blank" href="<?php $this->info("yt_link") ?>" class="socialico">X</a>
					<a target="_blank" href="<?php $this->info("rss_link") ?>" class="socialico">R</a>
				</p>
				<p class="silver"><img src="public/images/scnt.png" alt="" class="company-logo" /><a href="https://silverconnectwebdesign.com/website-development" rel="external" target="_blank"> Web Design</a> Done by <a href="https://silverconnectwebdesign.com" rel="external" target="_blank">Silver Connect Web Design</a></p>
			</div>
		</div>
	</div>
</footer>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo URL; ?>public/scripts/sendform.js" data-view="<?php echo $view; ?>" id="sendform"></script>
<!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>  -->
<script src="<?php echo URL; ?>public/scripts/responsive-menu.js"></script>
<script src="https://unpkg.com/sweetalert2@7.20.10/dist/sweetalert2.all.js"></script>

<?php if( $this->siteInfo['cookie'] ): ?>
	<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
	<script>
	window.addEventListener("load", function(){
	window.cookieconsent.initialise({
	  "palette": {
	    "popup": {
	      "background": "#000"
	    },
	    "button": {
	      "background": "#3085d6"
	    }
	  }
	})});
	</script>
<?php endif ?>

<?php if(in_array($view,["home","contact"])): ?>
	<script src='//www.google.com/recaptcha/api.js?onload=captchaCallBack&render=explicit' async defer></script>
	<script>
		var captchaCallBack = function() {
			$('.g-recaptcha').each(function(index, el) {
				grecaptcha.render(el, {'sitekey' : '<?php $this->info("site_key");?>'});
			});
		};

		$('.consentBox').click(function () {
		    if ($(this).is(':checked')) {
		    	if($('.termsBox').length){
		    		if($('.termsBox').is(':checked')){
		        		$('.ctcBtn').removeAttr('disabled');
		        	}
		    	}else{
		        	$('.ctcBtn').removeAttr('disabled');
		    	}
		    } else {
		        $('.ctcBtn').attr('disabled', true);
		    }
		});

		$('.termsBox').click(function () {
		    if ($(this).is(':checked')) {
	    		if($('.consentBox').is(':checked')){
	        		$('.ctcBtn').removeAttr('disabled');
	        	}
		    } else {
		        $('.ctcBtn').attr('disabled', true);
		    }
		});

	</script>

<?php endif; ?>


<?php if ($view == "gallery"): ?>
	<script type="text/javascript" src="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
	<script type="text/javascript" src="<?php echo URL; ?>public/scripts/jquery.pajinate.js"></script>
	<script>
		$('#gall1').pajinate({ num_page_links_to_display : 3, items_per_page : 12 });
		$('.fancy').fancybox({
			helpers: {
				title : {
					type : 'over'
				}
			}
		});
	</script>
<?php endif; ?>

<a class="cta" href="tel:<?php $this->info("phone") ;?>"></a>

<?php $this->checkSuspensionFooter(); ?>
</body>
</html>
