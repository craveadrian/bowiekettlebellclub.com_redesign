<div id="content">
	<div id="our-story-section">
		<div class="row">
			<div class="osLeft inb">
				<h1>OUR STORY</h1>
				<h3>NATIONAL COUNCIL OF STRENGTH AND FITNESS (NCSF)</h3>
				<p>Philippians 2:3; Let nothing be done through strife or vainglory; but in lowliness of mind let each esteem other better than themselves.</p>
				<p>Come be a part of the best workout program on the planet. At the Bowie Kettlebell Club you’ll experience a unique fitness blend that encompasses elements of YOGA, TAI CHI, MARTIAL ARTS and AUTHENTIC RUSSIAN KETTLEBELLS. The “Morning Workout” will renew your creaking joints and wake up your aging nervous system.</p>
				<img src="public/images/content/ossImage1.jpg" alt="2 guys">
			</div>
			<div class="osRight inb">
				<img src="public/images/content/ossImage2.jpg" alt="Weights">
				<p>At Bowie Kettlebell Club in Bowie, Maryland, we offer an authentic Russian approach to fitness training using the traditional kettlebell and self-defense classes.We service clients in Bowie and the surrounding communities, sharing more than 30 years of experience helping people like you learn new things to keep themselves healthy, flexible, and strong. Our jont mobility class provides a way to rejuvenate the body through a series of simple exercises everyone can learn.</p>
				<p>Chief Instructor Marvin King has held the Russian Kettlebell Certification (RKC) since 2006. He has been an active Personal Trainer certified through the National Council of Strength Fitness (NCSF) since 1999. Additionally he is well versed in a variety of Martial Arts and other self-defense instruction based on principles founded in Tim Larkin’s system for self protection.</p>
			</div>
		</div>
	</div>
	<div id="bowie-kettlebell-club-section">
		<div class="row">
			<div class="bkcsLeft inb">
				<h1>BOWIE <br/> KETTLEBELL CLUB</h1>
				<p>Come be a part of the best workout program on the planet. At the Bowie Kettlebell Club you’ll experience a unique fitness blend that encompasses elements of YOGA, TAI CHI, MARTIAL ARTS and AUTHENTIC RUSSIAN KETTLEBELLS. The “Morning Workout” will renew your creaking joints and wake up your aging nervous system.</p>
				<a href="<?php echo URL ?>services#content" class="btn">READ MORE</a>
			</div>
			<div class="bkcsRight inb">
				<img src="public/images/content/bkcsImage.jpg" alt="Trainers">
			</div>
		</div>
	</div>
	<div id="kettlebell-training-section">
		<div class="row">
			<div class="ktsLeft inb">
				<img src="public/images/content/ktsImage.jpg" alt="Trainer">
			</div>
			<div class="ktsRight inb">
				<h1>KETTLEBELL </br> TRAINING</h1>
				<p>At the Bowie Kettlebell you'll be introduced to progressive strength training by way of the Kettlebell. Through proper training at the Bowie Kettlebell Club you'll learn and understand how all purpose strength is achived and maintained for a better quality of life.</p>
				<a href="<?php echo URL ?>services#content" class="btn">READ MORE</a>
			</div>
		</div>
	</div>
	<div id="protect-yourself-section">
		<div class="row">
			<div class="pysLeft inb">
				<h1>PROTECT <br/> YOURSELF</h1>
				<p>Come be a part of the best workout program on the planet. At the Bowie Kettlebell Club you’ll experience a unique fitness blend that encompasses elements of YOGA, TAI CHI, MARTIAL ARTS and AUTHENTIC RUSSIAN KETTLEBELLS. The “Morning Workout” will renew your creaking joints and wake up your aging nervous system.</p>
				<a href="<?php echo URL ?>services#content" class="btn">READ MORE</a>
			</div>
			<div class="pysRight inb">
				<img src="public/images/content/pysImage.jpg" alt="Training">
			</div>
		</div>
	</div>
	<div id="our-gallery-section">
		<div class="row">
			<h1>OUR GALLERY</h1>
			<div class="gallery-images">
				<img src="public/images/content/galleryImage1.jpg" alt="Gallery Image" class="inb">
				<img src="public/images/content/galleryImage2.jpg" alt="Gallery Image" class="inb">
				<img src="public/images/content/galleryImage3.jpg" alt="Gallery Image" class="inb">
			</div>
			<a href="<?php echo URL ?>gallery#content" class="btn">VIEW MORE</a>
		</div>
	</div>
</div>
